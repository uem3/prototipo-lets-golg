Hello and thank you for purchasing Low Poly Minigolf package.

To make the scenes match screenshots you can change the Color Space to Linear. >Project Settings>Player>
If the shadows are not showing then Generate Lighting. Auto Generation is set of. 
Also check >Project Settings>Quality>Shadow Distance  (set to 50)

There are 46 prefabs. All models use one atlas texture.

Texture is 512*512 png

For support contact: a3deve@gmail.com